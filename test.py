# -*- coding: utf-8 -*
import os  
import time
# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')
# 1. [Content] Side Bar Text
def test_side_bar_text():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1
	assert xmlString.find('查訂單/退訂退款') != -1
	assert xmlString.find('追蹤/買過/看過清單') != -1
	assert xmlString.find('智慧標籤') != -1
	assert xmlString.find('PChome 旅遊') != -1
	assert xmlString.find('線上手機回收') != -1
	assert xmlString.find('給24h購物APP評分') != -1
# 2. [Screenshot] Side Bar Text
def test_side_bar_screen():
	os.system('adb shell screencap -p /sdcard/2.png && adb pull /sdcard/2.png ./Screenshot/2.png')
# 3. [Context] Categories
def test_categories_text():
	os.system('adb shell input tap 1000 1000')
	os.system('adb shell input swipe 800 800 800 300 ')
	os.system('adb shell input tap 1000 1000')
	os.system('adb wait-for-device')
	time.sleep(5)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('精選') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1
# 4. [Screenshot] Categories
def test_categories_screen():
	os.system('adb shell screencap -p /sdcard/4.png && adb pull /sdcard/4.png ./Screenshot/4.png')
# 5. [Context] Categories page
def test_categories_page_text():
	os.system('adb shell input tap 1000 1000')
	os.system('adb shell input tap 100 100')
	os.system('adb shell input tap 100 550')
	os.system('adb wait-for-device')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1
# 6. [Screenshot] Categories page
def test_categories_page_screen():
	os.system('adb shell screencap -p /sdcard/6.png && adb pull /sdcard/6.png ./Screenshot/6.png')
# 7. [Behavior] Search item “switch”
def test_search():
	os.system('adb shell input tap 500 100')
	os.system('adb shell input text "switch"')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('switch') != -1
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	os.system('adb wait-for-device')
	time.sleep(5)
# 8. [Behavior] Follow an item and it should be add to the list
def test_follow():
	os.system('adb shell input tap 1000 400')
	os.system('adb shell input tap 100 1750')
	os.system('adb wait-for-device')
	time.sleep(5)
	os.system('adb shell input tap 100 100')
	os.system('adb wait-for-device')
	time.sleep(5)
	os.system('adb shell input tap 100 800')
	os.system('adb wait-for-device')
	time.sleep(5)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('Switch') != -1
# 9. [Behavior] Navigate to the detail of item
def test_detail():
	os.system('adb shell input tap 100 1000')
	os.system('adb wait-for-device')
	time.sleep(5)
	os.system('adb shell input swipe 800 1000 800 300 ')
	os.system('adb shell input tap 500 100 ')
	time.sleep(5)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('商品特色') != -1
# 10. [Screenshot] Disconnetion Screen
def test_disconnetion_screen():
	os.system('adb shell input tap 100 100')
	os.system('adb shell input tap 100 100')
	os.system('adb shell input tap 500 1700')
	os.system('adb shell screencap -p /sdcard/10.png && adb pull /sdcard/10.png ./Screenshot/10.png')



